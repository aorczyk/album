function jsUcfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

Vue.component('spinner', {
  template: `
    <div class="modal is-active">
      <div class="modal-background"></div>
      <div class="modal-content" style="overflow: hidden;">
        <div class="sk-folding-cube">
          <div class="sk-cube1 sk-cube"></div>
          <div class="sk-cube2 sk-cube"></div>
          <div class="sk-cube4 sk-cube"></div>
          <div class="sk-cube3 sk-cube"></div>
        </div>
      </div>
    </div>
    `
});