Vue.component('top-bar', {
  data() {
    return {
      sharedState: store.state,
      privateState: {}
    }
  },
  methods: {
    search() {
      store.search();
    },
    clearSearch() {
      store.clearSearch();
    },
    save() {
      var dataToSave = {
        osoby: this.sharedState.album.osoby,
        zdjecia: this.sharedState.album.zdjecia,
      };

      var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent('var dane = ' + JSON.stringify(dataToSave));
      var dlAnchorElem = this.$refs.download;
      dlAnchorElem.setAttribute("href", dataStr);
      dlAnchorElem.setAttribute("download", "Dane.js");
      dlAnchorElem.click();
      removeBeforeunload();
      this.sharedState.hasUnsavedChanges = false;
    },
    newPicture() {
      this.sharedState.pictureOpened = {
        id: null,
        plik: null,
        opis: '',
        osoby: [],
        new: true
      }
    },
    newPerson() {
      this.sharedState.personOpened = {
        id: '',
        imie: '',
        nazwisko: '',
        polacz: null
      }
    }
  },
  watch: {
    'sharedState.searchText': function (newVal, oldVal) {
      // console.log('searchText', newVal);
      // if (this.sharedState.searched && newVal !== oldVal){
      //   Vue.set(this.sharedState, 'searched', null);
      // }
      if (newVal === '') {
        this.clearSearch();
      }
    }
  },
  template: `
  <div>
    <image-modal v-if="sharedState.pictureOpened"/>
    <person-modal v-if="sharedState.personOpened"/>

    <nav class="navbar is-white is-fixed-top top-bar" role="navigation" aria-label="main navigation">
      <a ref="download" style="display:none"></a>
      <div class="navbar-brand">
        <strong class="navbar-item">
            <i class="fas fa-camera-retro"></i>
        </strong>
      </div>
      <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
          <router-link class="navbar-item" to="/album"><b>Album</b></router-link>
          <!--<router-link class="navbar-item" to="/album">Zdjęcia</router-link>-->
          <router-link class="navbar-item" to="/osoby">Osoby</router-link>

          <div class="navbar-item" v-if="sharedState.searchVisible && sharedState.forum && !sharedState.forum.accessDenied">
            <div class="field has-addons">
              <div class="control">
                <input class="input" type="text" placeholder="Search" v-model="sharedState.searchText" @keyup.enter="search">
              </div>
              <!--<div class="control" v-if="sharedState.searched">
                <a class="button is-info" @click="clearSearch">
                  <span class="icon is-small">
                    <i class="fas fa-times"></i>
                  </span>
                </a>
              </div>
              -->
              <div class="control">
                <a class="button is-light" @click="search">
                  <span class="icon is-small has-text-grey-light">
                    <i class="fas fa-search"></i>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="navbar-end">
          <div class="navbar-item">
            <div class="buttons">
              <a class="button" @click.stop="newPerson">
                <span class="icon is-small">
                  <i class="fas fa-user-plus"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="navbar-item">
            <div class="buttons">
              <a class="button" @click.stop="newPicture">
                <span class="icon is-small">
                  <i class="far fa-image"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="navbar-item">
            <div class="buttons">
              <a class="button" :class="{'is-danger': sharedState.hasUnsavedChanges}" @click.stop="save">
                Zapisz
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
`
});