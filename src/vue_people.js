var compPeople = Vue.component('people', {
  data() {
    return {
      sharedState: store.state,
      searchText: ''
    }
  },
  computed: {
    people() {
      let out = this.sharedState.album.osoby;

      out.sort((a, b)=>{
        let aKey = a.nazwisko + a.imie;
        let bKey = b.nazwisko + b.imie;

        return aKey > bKey ? 1 : aKey < bKey ? -1 : 0;
      })

      return out;
    },
  },
  methods: {
    showPictures(id){
      //`/${this.state.forum.id}`

      let person = this.sharedState.album.peopleById[id];
      if (person.polacz){
        router.push(`/album/${person.polacz}`)
      } else {
        router.push(`/album/${id}`)
      }
    },
    edit(item) {
      this.sharedState.personOpened = item;
    },
    remove(item) {
      let index = this.sharedState.album.osoby.findIndex((x) => {
        return item.id == x.id
      });

      if (index != -1){
        this.sharedState.album.osoby.splice(index, 1);
      }

      if (this.sharedState.album.picturesByPersonId[item.id]){
        for (let pic of this.sharedState.album.picturesByPersonId[item.id]){
          let index = pic.osoby.findIndex((x) => {
            return item.id == x.id
          });
  
          if (index != -1){
            pic.osoby.splice(index, 1);
          }
        }

        delete this.sharedState.album.picturesByPersonId[item.id];
      }


      let connected = this.sharedState.album.osoby.filter((x)=>{
        return x.polacz == item.id;
      })

      for (let person of connected){
        person.polacz = null;
      }

      delete this.sharedState.album.peopleById[item.id];
      store.unsavedChanges();
    },
    getPersonById(id) {
      let person = this.sharedState.album.peopleById[id];
      if (person){
        return person.imie + ' ' + person.nazwisko;
      }

      return '';
    }
  },
  template: `
    <div class="people-page">
      <div class="container">
        <table class="table is-hoverable">
          <thead>
            <tr>
              <td>Nazwisko</td>
              <td>Imię</td>
              <td>Skojarzona z</td>
              <td>Zdjęcia</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            <tr v-for="person in people" :key="person.id" @click="showPictures(person.id)">
              <td>{{ person.nazwisko }}</td>
              <td>{{ person.imie }}</td>
              <td>{{ getPersonById(person.polacz) }}</td>
              <td>{{ sharedState.album.picturesByPersonId[person.id] ? sharedState.album.picturesByPersonId[person.id].length : 0 }}</td>
              <td>
                <button class="button is-light is-small" @click.stop="edit(person)" title="Edytuj osobę">
                  <span class="icon is-small">
                    <i class="fas fa-pen"></i>
                  </span>
                </button>
                <button class="button is-light is-small" @click.stop="remove(person)" title="Usuń osobę">
                  <span class="icon is-small">
                    <i class="fas fa-trash-alt"></i>
                  </span>
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    `
})