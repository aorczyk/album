var compPersonModal = Vue.component('person-modal', {
  data() {
    return {
      sharedState: store.state,
      edit: true
    }
  },
  created() {
    this.sharedState.searchVisible = true;

    if (!this.sharedState.personOpened.id){
      this.sharedState.personOpened.id = new Date().getTime();
      this.edit = false;
    }
  },
  methods: {
    close() {
      this.sharedState.personOpened = null;
    },
    save() {
      if (!this.sharedState.album.peopleById[this.sharedState.personOpened.id]) {
        let newPerson = JSON.parse(JSON.stringify(this.sharedState.personOpened));
        this.sharedState.album.osoby.push(newPerson);
        this.sharedState.album.peopleById[this.sharedState.personOpened.id] = newPerson;

        this.sharedState.album.osoby.sort((a, b) => {
          let aKey = a.nazwisko + a.imie;
          let bKey = b.nazwisko + b.imie;

          return aKey > bKey ? 1 : aKey < bKey ? -1 : 0;
        })
      }

      this.close();
      store.unsavedChanges();
    },
  },
  template: `
    <div class="modal" :class="{'is-active': true}" @click.stop="">
      <div class="modal-background"></div>
      <div class="modal-card" @click.stop="">
        <header class="modal-card-head">
          <p class="modal-card-title">Osoba</p>
        </header>
        <section class="modal-card-body">
          <div class="field">
            <label class="label">Id</label>
            <div class="control">
              <input class="input" type="text"
                placeholder="Unikalny identyfikator" v-model="sharedState.personOpened.id" :disabled="edit">
            </div>
          </div>
          <div class="field">
            <label class="label">Imię</label>
            <div class="control">
              <input class="input" type="text"
                placeholder="" v-model="sharedState.personOpened.imie">
            </div>
          </div>
          <div class="field">
            <label class="label">Nazwisko</label>
            <div class="control">
              <input class="input" type="text"
                placeholder="" v-model="sharedState.personOpened.nazwisko">
            </div>
          </div>
          <div class="field">
            <div class="control">
                <label class="label">Skojarz z</label>
                <div class="select">
                    <select v-model="sharedState.personOpened.polacz">
                    <option>Wybierz osobę</option>
                    <option :value="person.id" v-for="person in sharedState.album.osoby">{{ person.nazwisko }}
                        {{ person.imie }}</option>
                    </select>
                </div>
            </div>
          </div>
        </section>
        <footer class="modal-card-foot">
          <div class="field is-grouped">
            <div class="control">
              <button class="button is-info" @click.stop="save" :disabled="!sharedState.personOpened.id || !sharedState.personOpened.imie || !sharedState.personOpened.nazwisko">Dodaj</button>
            </div>
            <div class="control">
              <button class="button" @click.stop="close">Anuluj</button>
            </div>
          </div>
        </footer>
      </div>
    </div>
      `
});