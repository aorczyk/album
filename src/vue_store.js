var store = {
  debug: true,
  state: {
    album: dane,
    isSpinnerVisible: false,
    searched: null,
    searchText: '',
    searchVisible: true,
    pictureOpened: null,
    personOpened: null,
    hasUnsavedChanges: false
  },
  dataRecalculate(){
    this.state.isSpinnerVisible = true;

    let dane = this.state.album;
    // Data preprocess
    dane.peopleById = {};

    for (let item of dane.osoby) {
      dane.peopleById[item.id] = item;
    }

    dane.picturesByPersonId = {};

    for (let pict of dane.zdjecia) {
      for (let person of pict.osoby){
        if (!dane.picturesByPersonId[person.id]){
          dane.picturesByPersonId[person.id] = []
        }

        dane.picturesByPersonId[person.id].push(pict);

        let connectedPersonId = dane.peopleById[person.id].polacz;
        if (connectedPersonId){
          if (!dane.picturesByPersonId[connectedPersonId]){
            dane.picturesByPersonId[connectedPersonId] = []
          }
          dane.picturesByPersonId[connectedPersonId].push(pict);
        }
      }
    }

    dane.osoby.sort((a, b)=>{
      let aKey = a.nazwisko + a.imie;
      let bKey = b.nazwisko + b.imie;

      return aKey > bKey ? 1 : aKey < bKey ? -1 : 0;
    })

    Vue.set(this.state, 'album', dane);
    this.state.isSpinnerVisible = false;
  },
  search() {
    if (!this.state.searchText) {
      this.state.searched = null;
      return;
    }

    var reg = new RegExp(this.state.searchText, 'i');
    this.state.isSpinnerVisible = true;

    this.state.searched = this.state.forum.questions.filter((x) => {
      return x.title.search(reg) != -1 || x.body.search(reg) != -1;
    }).map((x) => {
      return x.id
    });

    for (var entity of ['answers', 'comments']) {
      for (var id in this.state.forum[entity]) {
        for (var item of this.state.forum[entity][id]) {
          if (item.body && item.body.search(reg) != -1) {
            this.state.searched.push(item.qId);
          }
        }
      }
    }

    this.state.isSpinnerVisible = false;

    if (router.currentRoute.path != `/${this.state.forum.id}`) {
      router.push(`/${this.state.forum.id}`);
    }
  },
  clearSearch() {
    this.state.searchText = '';
    // this.search();
    Vue.set(this.state, 'searched', null);
  },
  highlight(string) {
    var out = string;

    if (this.state.searchText && this.state.searched) {
      const regex = />([^><]*?)</gi;
      const regexSearch = new RegExp(this.state.searchText, 'ig');

      var match = regex.exec(string);

      while (match != null) {
        let outPart = '>' + match[1].replace(regexSearch, '<span class="highlight">$&</span>') + '<';

        if (outPart) {
          out = out.replace(match[0], outPart);
        }

        match = regex.exec(string);
      }
    }
    return out;
  },
  highlightText(string) {
    var out = string;

    if (this.state.searchText && this.state.searched) {
      const regexSearch = new RegExp(this.state.searchText, 'ig');

      out = string.replace(regexSearch, '<span class="highlight">$&</span>');
    }

    return out;
  },
  unsavedChanges() {
    this.state.hasUnsavedChanges = true;
    addBeforeunload();
  }
};