var compImageModal = Vue.component('image-modal', {
  data() {
    return {
      sharedState: store.state,
      quillSettings: quillSettings,
      pointerVisible: false,
      pointer: {
        pos1: 0,
        pos2: 0,
        pos3: 0,
        pos4: 0,
        x: 0,
        y: 0,
        b: 40
      },
      newPersonFormVisible: false,
      newPersonId: null,
      personEdit: null,
      hoveredPersonId: null,
      descriptionEditVisible: false
    }
  },
  created() {
    this.sharedState.searchVisible = true;
  },
  methods: {
    pictureOpen(item) {
      this.sharedState.pictureOpened = item;
    },
    pictureClose() {
      this.sharedState.pictureOpened = null;
    },
    dragMouseDown(e) {
      console.log('dragMouseDown');

      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      this.pointer.pos3 = e.clientX;
      this.pointer.pos4 = e.clientY;
      document.onmouseup = this.closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = this.elementDrag;
    },
    closeDragElement(event) {
      /* stop moving when mouse button is released:*/

      if (!document.onmouseup) {
        return;
      }

      document.onmouseup = null;
      document.onmousemove = null;
      store.unsavedChanges();
    },
    elementDrag(e) {
      let elmnt = e.target;

      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      this.pointer.pos1 = this.pointer.pos3 - e.clientX;
      this.pointer.pos2 = this.pointer.pos4 - e.clientY;
      this.pointer.pos3 = e.clientX;
      this.pointer.pos4 = e.clientY;
      // set the element's new position:
      let top = (elmnt.offsetTop - this.pointer.pos2);
      let left = (elmnt.offsetLeft - this.pointer.pos1);

      Vue.set(this.pointer, 'x', left);
      Vue.set(this.pointer, 'y', top);
    },
    // pointerMove(event){
    //     let scrollTop = event.target.parentElement.scrollTop;
    //     let windowScrollTop = window.scrollY;

    //     console.log('ws', windowScrollTop);
    //     console.log('s', scrollTop);

    //     let imgH = event.target.offsetHeight;
    //     let imgW = event.target.offsetWidth;

    //     let parentImgH = event.target.parentElement.offsetHeight;
    //     let parentImgW = event.target.parentElement.offsetWidth;

    //     console.log('pI', parentImgH);

    //     const rect = event.target.getBoundingClientRect();
    //     let left = event.pageX - rect.left;
    //     let top = event.pageY - rect.top - windowScrollTop + scrollTop;

    //     Vue.set(this.pointer, 'x', left);
    //     Vue.set(this.pointer, 'y', top);

    //     let leftPercent = left * 100 / imgW;
    //     let topPercent = top * 100 / imgH;

    //     Vue.set(this.pointer, 'x', leftPercent);
    //     Vue.set(this.pointer, 'y', topPercent);

    //     console.log('>', left, top);
    //     console.log('I', imgW, imgH);
    //     console.log('%', leftPercent, topPercent);
    // },
    showNewPersonForm() {
      this.pointerVisible = false;
      this.pointer.x = 0;
      this.pointer.y = 0;
      this.pointer.b = 40;

      this.newPersonFormVisible = true;
    },
    closeNewPersonForm() {
      this.pointerVisible = false;
      this.newPersonFormVisible = false;
    },
    savePerson() {
      let newData = {
        id: this.newPersonId,
      };

      if (this.pointerVisible) {
        let img = this.$refs.image;
        let left = this.pointer.x * 100 / img.offsetWidth;
        let top = this.pointer.y * 100 / img.offsetHeight;

        newData.x = left;
        newData.y = top;
        newData.b = this.pointer.b;

        if (this.personEdit) {
          this.personEdit.x = left;
          this.personEdit.y = top;
          this.personEdit.b = this.pointer.b;
        }
      } else {
        if (this.personEdit) {
          delete this.personEdit.x;
          delete this.personEdit.y;
          delete this.personEdit.z;
        }
      }

      if (this.personEdit) {
        this.personEdit.id = this.newPersonId;
        this.personEdit = null;
      } else {
        this.sharedState.pictureOpened.osoby.push(JSON.parse(JSON.stringify(newData)))
      }

      this.closeNewPersonForm();
      store.unsavedChanges();
    },
    editPerson(person) {
      this.personEdit = person;
      this.pointerVisible = person.hasOwnProperty('x');

      let img = this.$refs.image;

      if (person.hasOwnProperty('x')){
        this.pointer.x = person.x * img.offsetWidth / 100;
        this.pointer.y = person.y * img.offsetHeight / 100;
        this.pointer.b = person.b;
      } else {
        this.pointer.x = 0;
        this.pointer.y = 0;
        this.pointer.b = 40;
      }

      this.newPersonId = person.id;

      this.newPersonFormVisible = true;
      store.unsavedChanges();
    },
    removePerson() {
      let index = this.sharedState.pictureOpened.osoby.indexOf((x) => {
        return this.personEdit == x.id
      });
      this.sharedState.pictureOpened.osoby.splice(index, 1);
      this.personEdit = null;
      this.closeNewPersonForm();
      store.unsavedChanges();
    },
    togglePointer() {
      this.pointerVisible = !this.pointerVisible;
    },
    tagHover(person) {
      this.hoveredPersonId = person.id;
    },
    tagLeave(person) {
      this.hoveredPersonId = null;
    },
    previewFile(event) {
      console.log(event.target.files);
      this.sharedState.pictureOpened.plik = './Zdjęcia/' + event.target.files[0].name;
      this.sharedState.pictureOpened.id = event.target.files[0].name.split('.')[0];
    },
    toggleDescriptionEdit(){
      this.descriptionEditVisible = !this.descriptionEditVisible;
      store.unsavedChanges();
    },
    savePicture() {
      delete this.sharedState.pictureOpened.new;

      let img = this.$refs.image;

      this.sharedState.pictureOpened.offsetWidth = img.offsetWidth;
      this.sharedState.pictureOpened.offsetHeight = img.offsetHeight;

      // this.sharedState.pictureOpened.id = new Date().getTime();
      this.sharedState.album.zdjecia.push(JSON.parse(JSON.stringify(this.sharedState.pictureOpened)));
      this.sharedState.pictureOpened = null;
      store.unsavedChanges();
    },
  },
  template: `
  <div class="modal" :class="{'is-active': true}" @click.stop="pictureClose" v-if="sharedState.pictureOpened">
    <div class="modal-background"></div>
    <div class="modal-content" @click.stop="">
      <div class="modal-content-body">
        <div class="modal-content-image" @click.stop="" v-if="sharedState.pictureOpened.plik">
          <img ref="image" :src="sharedState.pictureOpened.plik">
          <div class="pointer pointer-select"
            :style="{'left': pointer.x + 'px', 'top': pointer.y + 'px', 'width': pointer.b + 'px', 'height': pointer.b + 'px'}"
            @mousedown="dragMouseDown" @mouseleave="closeDragElement" v-if="pointerVisible"></div>
          <div class="pointer"
            :style="{'top': person.y + '%', 'left': person.x + '%', 'width': person.b + 'px', 'height': person.b + 'px'}"
            v-for="person in sharedState.pictureOpened.osoby" v-if="person.x && person.y" @mouseover="tagHover(person)"
            @mouseleave="tagLeave(person)">
            <div class="pointer-content" :class="{'pointer-hover': person.id == hoveredPersonId}"></div>
            <div class="pointer-title" :class="{'pointer-title-hover': person.id == hoveredPersonId}">
              {{ sharedState.album.peopleById[person.id].imie }}
              {{ sharedState.album.peopleById[person.id].nazwisko }}</div>
          </div>
        </div>
        <div class="field" v-else>
          <div class="control">
            <input class="input" type="file" @change="previewFile">
          </div>
        </div>
        <div class="person-edit" v-if="newPersonFormVisible">
          <div class="field is-horizontal is-grouped">
            <div class="control">
              <div class="select is-small">
                <select v-model="newPersonId">
                  <option>Wybierz osobę</option>
                  <option :value="person.id" v-for="person in sharedState.album.osoby">{{ person.nazwisko }}
                    {{ person.imie }}</option>
                </select>
              </div>
            </div>
            <div class="control">
              <button class="button is-small" :class="{'is-link': pointerVisible}" @click.stop="togglePointer"
                title="Dodaj/Usuń zaznaczenie">
                <span class="icon is-small">
                  <i class="fas fa-vector-square"></i>
                </span>
              </button>
            </div>
            <div class="control" v-if="pointerVisible">
              <div class="select is-small">
                <input class="input is-info is-small" type="number" min="10" max="100" v-model="pointer.b">
              </div>
            </div>
            <div class="control">
              <button class="button is-link is-small" @click.stop="savePerson" :disabled="!newPersonId" title="Dodaj osobę">
                <span class="icon is-small">
                  <i class="fas fa-check"></i>
                </span>
              </button>
            </div>
            <div class="control">
              <button class="button is-link is-light is-small" @click.stop="closeNewPersonForm" title="Anuluj">
                <span class="icon is-small">
                  <i class="fas fa-times"></i>
                </span>
              </button>
            </div>
            <div class="control" v-if="personEdit">
              <button class="button is-danger is-light is-small" @click.stop="removePerson" title="Usuń osobę">
                <span class="icon is-small">
                  <i class="fas fa-trash-alt"></i>
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-content-tags">
        <span class="tag is-light"
          :class="{'tag-hover': person.id == hoveredPersonId, 'tag-edit': personEdit && person.id == personEdit.id}"
          v-for="person in sharedState.pictureOpened.osoby" @mouseover="tagHover(person)" @mouseleave="tagLeave(person)" @click.stop="editPerson(person)">
            <span>{{ sharedState.album.peopleById[person.id].imie }} {{ sharedState.album.peopleById[person.id].nazwisko }}</span> 
          </span>
        <span class="tag is-light" @click.stop="showNewPersonForm"
          title="Nowa osoba"><span v-if="sharedState.pictureOpened.osoby.length">+</span><span v-else>Dodaj osobę</span></span>
      </div>
      <div class="modal-content-description" v-if="!descriptionEditVisible">{{ sharedState.pictureOpened.opis }}
        <button class="button is-light is-small" :class="{'is-link': descriptionEditVisible}"
          @click.stop="toggleDescriptionEdit" title="Edytuj opis">
          <span class="icon is-small" v-if="sharedState.pictureOpened.opis">
            <i class="fas fa-pen"></i>
          </span>
          <span v-else>Dodaj opis</span>
        </button>
      </div>
      <div v-else>
        <div class="field has-addons">
          <div class="control is-expanded">
            <input class="input" type="text" placeholder="Opis zdjęcia" v-model="sharedState.pictureOpened.opis">
          </div>
          <div class="control">
            <a class="button is-info" @click.stop="toggleDescriptionEdit">
              <span class="icon is-small">
                <i class="fas fa-check"></i>
              </span>
            </a>
          </div>
        </div>
        <!--<quill-editor v-model="sharedState.pictureOpened.opis" ref="quillEditorA" :options="quillSettings"></quill-editor>-->
      </div>
      <div class="field is-grouped" v-if="sharedState.pictureOpened.new">
        <div class="control">
          <button class="button is-link is-small" @click.stop="savePicture"
            :disabled="!sharedState.pictureOpened.plik">Zapis</button>
        </div>
        <div class="control">
          <button class="button is-link is-light is-small" @click.stop="pictureClose">Anuluj</button>
        </div>
      </div>
    </div>
    <button class="modal-close is-large" aria-label="close" @click.stop="pictureClose"></button>
  </div>
    `
});