const routes = [{
    path: '/album',
    component: compAlbum,
  },
  {
    path: '/album/:personId',
    component: compAlbum,
    props: true,
  },
  {
    path: '/osoby',
    component: compPeople,
  },
];

const router = new VueRouter({
  routes
});