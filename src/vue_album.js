var compAlbum = Vue.component('album', {
  props: ['personId'],
  data() {
    return {
      sharedState: store.state,
      quillSettings: quillSettings,
      newPicture: false,
      pointerVisible: false,
      pointer: {
        pos1: 0,
        pos2: 0,
        pos3: 0,
        pos4: 0,
        x: 0,
        y: 0,
        b: 40
      },
      newPersonFormVisible: false,
      newPersonId: null,
      personEdit: null,
      hoveredPersonId: null,
      descriptionEditVisible: false,
      recalculate: 1
    }
  },
  created() {
    this.sharedState.searchVisible = true;
  },
  computed: {
    pictures() {
      if (this.personId && this.recalculate) {
        // let out = this.sharedState.album.zdjecia.find((pict)=>{
        //   return -1 != pict.osoby.findIndex((p)=>{
        //     return p.id == this.personId;
        //   });
        // })
        // console
        return this.sharedState.album.picturesByPersonId[this.personId];
      } else {
        return this.sharedState.album.zdjecia;
      }
    },
    person() {
      if (this.personId) {
        let person = this.sharedState.album.peopleById[this.personId];
        let out = person.imie + ' ' + person.nazwisko;

        if (person.polacz){
          let personP = this.sharedState.album.peopleById[person.polacz];
          out += ' (' + person.imie + ' ' + person.nazwisko + ')';
        }

        return out;
      }

      return;
    }
  },
  methods: {
    pictureOpen(item) {
      this.sharedState.pictureOpened = item;
    },
    removePicture(picture) {
      let index = this.sharedState.album.zdjecia.findIndex((x) => {
        return picture.id == x.id
      });
      this.sharedState.album.zdjecia.splice(index, 1);

      if (this.personId){
        let index = this.sharedState.album.picturesByPersonId[this.personId].findIndex((x) => {
          return picture.id == x.id
        });

        this.sharedState.album.picturesByPersonId[this.personId].splice(index, 1);
        this.recalculate = new Date().getTime();
      }

      store.dataRecalculate();
      store.unsavedChanges();
    }
  },
  template: `
  <div>
    <div class="album-page">
      <div class="container">
        <div class="person" v-if="person">{{ person }}</div>
        <div class="person" v-else>Wszystkie zdjęcia</div>
        <div class="album">
          <div class="photo" :key="item.id" v-for="item in pictures">
            <button class="button is-white is-small photo-remove" @click.stop="removePicture(item)" title="Usuń zdjęcie">
              <span class="icon is-small">
                <i class="fas fa-trash-alt"></i>
              </span>
            </button>
            <img :src="item.plik" @click="pictureOpen(item)">
          </div>
        </div>
      </div>
    </div>
  </div>
  `
});